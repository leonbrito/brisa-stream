//
//  User.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 09/10/2017.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import Foundation

class User {
    
    private var _id:String!
    private var _auth_id:Int!
    private var _cliente_nome:String!
    private var _documento:String!
    private var _modulos:[Modulo]!
    private var _resetar_senha:Bool!
    private var _upload_termos:Bool!
    private var _valid:Int!
    
    var id: String {
        if _id == nil { _id = "" }
        return _id
    }
    
    var auth_id: Int {
        if _auth_id == nil { _auth_id = Int() }
        return _auth_id
    }
    
    var cliente_nome: String {
        if _cliente_nome == nil { _cliente_nome = "" }
        return _cliente_nome
    }
    
    var documento: String {
        if _documento == nil { _documento = "" }
        return _documento
    }
    
    var modulos: [Modulo] {
        if _modulos == nil { _modulos = [Modulo]() }
        return _modulos
    }
    
    var resetar_senha: Bool {
        if _resetar_senha == nil { _resetar_senha = Bool() }
        return _resetar_senha
    }
    
    var upload_termos: Bool {
        if _upload_termos == nil { _upload_termos = Bool() }
        return _upload_termos
    }
    
    var valid: Int {
        if _valid == nil { _valid = Int() }
        return _valid
    }
    
    init(userDict: Dictionary<String, AnyObject>) {
        
        if let id = userDict["id"] as? String {
            self._id = id
        }
        
        if let auth_id = userDict["auth_id"] as? Int {
            self._auth_id = auth_id
        }
        
        if let cliente_nome = userDict["cliente_nome"] as? String {
            self._cliente_nome = cliente_nome
        }
        
        if let documento = userDict["documento"] as? String {
            self._documento = documento
        }
        
        if let documento = userDict["documento"] as? String {
            self._documento = documento
        }
        
        self._modulos = [Modulo]()
        if let mods = userDict["modulos"] as? [Dictionary<String, AnyObject>] {
            for obj in mods {
                let modulo = Modulo(moduloDict: obj)
                self._modulos.append(modulo)
            }
        }
        
        if let resetar_senha = userDict["resetar_senha"] as? Bool {
            self._resetar_senha = resetar_senha
        }
        
        if let upload_termos = userDict["upload_termos"] as? Bool {
            self._upload_termos = upload_termos
        }
        
        if let valid = userDict["valid"] as? Int {
            self._valid = valid
        }
        
    }
}
