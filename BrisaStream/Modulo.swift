//
//  Modulo.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 09/10/2017.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import Foundation

class Modulo {
    
    private var _acesso:Bool!
    private var _modulo:String!
    
    var acesso: Bool {
        if _acesso == nil { _acesso = Bool() }
        return _acesso
    }
    
    var modulo: String {
        if _modulo == nil { _modulo = "" }
        return _modulo
    }
    
    init(moduloDict: Dictionary<String, AnyObject>) {
        if let acesso = moduloDict["acesso"] as? Bool {
            self._acesso = acesso
        }

        if let modulo = moduloDict["modulo"] as? String {
            self._modulo = modulo
        }
    }
    
}
