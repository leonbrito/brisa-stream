//
//  LoginVC.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 27/09/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {

    var user : User?
    let userDefaults = UserDefaults.standard
    
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var usuarioField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(LoginVC.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        self.hideKeyboard()
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func setCookies(cookies: HTTPCookie){
//        if let url = URL(string: LOGIN_URL) {
//            Alamofire.SessionManager.default.session.configuration.httpCookieStorage?.setCookies([cookies], for: url, mainDocumentURL: nil)
//        }
//    }
    
    func authenticate(completed: @escaping DownloadComplete) {
        if let user =  usuarioField.text {
            if let password = passwordField.text {
                
                let parameters: Parameters = [
                    "documento": user.trimmingCharacters(in: .whitespacesAndNewlines),
                    "password": password.trimmingCharacters(in: .whitespacesAndNewlines)
                ]
                
                Alamofire.request("\(LOGIN_URL)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                    
                    switch response.result {
                        case .success:
                            let result = response.result
                                                        
                            if let obj = result.value as? Dictionary<String, AnyObject> {
                                self.user = User(userDict: obj)
                            }
                            
                            self.userDefaults.set(true, forKey: "loginStatus")
                            self.userDefaults.synchronize()
                            
                            self.dismiss(animated: true, completion: nil)
                            self.performSegue(withIdentifier: "PlayerVC", sender: self.user)
                        
                        case .failure(let error):
                            print(error)
                            let alert = UIAlertController(title: "Problema de autenticação!", message: "Verifique se o usuário e senha estão corretos e tente novamente!", preferredStyle: UIAlertControllerStyle.alert)
                    
                            let addAction = UIAlertAction(title: "Ok", style: .default , handler:  { alertAction in
                                alert.dismiss(animated: true, completion: nil)
                            })
                        
                            alert.addAction(addAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                        completed()
                    }
            }
        }
    }

    @IBAction func authentication(_ sender: Any) {
        authenticate {}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let navController = segue.destination as? UINavigationController {
            if let channelStream = navController.viewControllers.first as? ChannelStreamVC {
                if segue.identifier == "PlayerVC" {
                    if let user = self.user {
                        channelStream.user = user
                    }
                }
            }
        }
    }
}
