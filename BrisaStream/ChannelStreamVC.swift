//
//  ChannelStreamVC.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 28/08/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import Alamofire
import SideMenu
import GoogleCast
import ReachabilitySwift

class ChannelStreamVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate ,GCKDeviceScannerListener, GCKDeviceManagerDelegate, GCKMediaControlChannelDelegate, UIActionSheetDelegate {
    
    private let kCancelTitle = "Cancelar"
    private let kDisconnectTitle = "Desconectar"
    
//    private let kReceiverAppID = "794B7BBF" // Text
    private let kReceiverAppID = "CC1AD845" // Android
//    private let kReceiverAppID = "89D11D90" // Brisas

    private var deviceScanner:GCKDeviceScanner?
    private var deviceManager:GCKDeviceManager?
    private var mediaInformation:GCKMediaInformation?
    private var selectedDevice:GCKDevice?
    private var mediaControlChannel: GCKMediaControlChannel
    @IBOutlet var messageTextField: UITextField!
    @IBOutlet var googleCastButton: UIBarButtonItem!
    
    
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var channelCollection: UICollectionView!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var logout: UIBarButtonItem!
    
    var user: User!
    private var isPlaying: Bool = false
    private var channels = [Channel]()
    private var channel: Channel? = nil
    private var asset : AVAsset? = nil
    private var playerItem: AVPlayerItem? = nil
    private var player: AVPlayer? = nil
    private var playerLayer = AVPlayerLayer()
    
    let activityIndicatiorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    required init(coder aDecoder: NSCoder) {
        // Establish filter criteria.
        let filterCriteria = GCKFilterCriteria(forAvailableApplicationWithID: kReceiverAppID)
        
        // Initialize device scanner.
        deviceScanner = GCKDeviceScanner(filterCriteria: filterCriteria)
        // [END device-scanner]
        
        mediaControlChannel = GCKMediaControlChannel()
        super.init(coder: aDecoder)!
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initially hide the Cast button.
        self.navigationItem.rightBarButtonItems = [logout]
        
        if let deviceScanner = deviceScanner {
            deviceScanner.add(self)
            deviceScanner.startScan()
            deviceScanner.passiveScan = true
        }
        
        self.channelCollection.delegate = self
        self.channelCollection.dataSource = self
        
        // Setup Configuration
        setupSideMenu()
        gestureRecognizer()
        downloadChannelData{}
        
        // Connection
        connectionObserver()
    }
    
    //*****************************************************************
    // MARK: - Player
    //*****************************************************************
    
    override func viewWillAppear(_ animated: Bool) {
        self.playerLayer.frame = self.view.bounds
        self.playerLayer.zPosition = -1
        self.view.layer.name = "PlayerLayer"
        self.view.layer.addSublayer(playerLayer)
        
        self.btnPlayPause.layer.name = "BtnPlay"
        self.channelCollection.layer.name = "Collection"
        
//        self.view.addSubview(activityIndicatiorView)
//        activityIndicatiorView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
//        activityIndicatiorView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
//        activityIndicatiorView.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        activityIndicatiorView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.hideElements()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.playerLayer.removeFromSuperlayer()
    }
    
    func downloadChannelData(completed: @escaping DownloadComplete) {
        
        Alamofire.request("\(BASE_URL2)\(URL_CHANNELS2)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
                case .success:
                    let result = response.result
                    if let obj = result.value as? Dictionary<String, [AnyObject]> {
                        if let lists = obj["data"] {
                            for list in lists {
                                if let l = list as? Dictionary<String, AnyObject> {
                                    let channel = Channel(channelDict: l)
                                    self.channels.append(channel)
                                }
                            }
                        }
                    }
                    self.channel = self.channels[0]
                    if self.channel != nil {
                        self.playChannel()
                        self.channelCollection.reloadData()
                    }
                case .failure(let error):
                    print(error)
                    let alert = UIAlertController(title: "Erro!", message: "Não foi possível carregar as informações, tente acessar novamente mais tarde!", preferredStyle: UIAlertControllerStyle.alert)
                    let addAgainAction = UIAlertAction(title: "Tentar novamente", style: .default, handler: { alertAction in
                        self.downloadChannelData {}
                    })
                    
                    let addCancelAction = UIAlertAction(title: "\(self.kCancelTitle)", style: .cancel, handler: { alertAction in
                        alert.dismiss(animated: true, completion: nil)
                    })

                    // Add action to the controller
                    alert.addAction(addAgainAction)
                    alert.addAction(addCancelAction)
                    self.present(alert, animated: true, completion: nil)
                }
            completed()
        }
    }
    
    func playChannel() {
        if let url = self.channel?.url {
            self.asset = AVAsset.init(url: url)
            self.playerItem = AVPlayerItem(asset: self.asset!)
            self.player = AVPlayer(playerItem: self.playerItem)
            self.playerLayer.player = self.player
            self.play()
            
//            activityIndicatiorView.startAnimating()
            
            isPlayingUpdate()
            self.navigationItem.title = self.channel?.name
        }
    }
    
    func changePlayChannel() {
        if let url = self.channel?.url {
            print(url)
            self.asset = AVAsset.init(url: url)
            self.playerItem =  AVPlayerItem(asset: self.asset!)
            self.playerLayer.player?.replaceCurrentItem(with: self.playerItem)
            self.navigationItem.title = self.channel?.name
            
//            activityIndicatiorView.startAnimating()

        }
    }
    
    

    func gestureRecognizer() {
//        let channelsSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleChannelsSwipe))
        let channelsTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleChannelsTap))
        channelsTapRecognizer.cancelsTouchesInView = false
        
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(channelsTapRecognizer)
    }
    
    @objc func handleChannelsSwipe(recognizer: UISwipeGestureRecognizer) {
        self.channelCollection.isHidden = !(self.channelCollection.isHidden)
        
        self.navigationController?.isNavigationBarHidden = !(self.navigationController?.isNavigationBarHidden)!
        self.btnPlayPause.isHidden = !(self.btnPlayPause.isHidden)
    }
    
    @objc func handleChannelsTap(recognizer: UISwipeGestureRecognizer) {
        let view = recognizer.view
        let loc = recognizer.location(in: view)
        let subView = view?.hitTest(loc, with: nil)
        
        print(subView?.layer.name)
    
        if subView?.layer.name == "PlayerLayer" {
            self.hideElements()
            //        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            //            self.hideElements()
            //        }
            Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(timeHideElements), userInfo: nil, repeats: false)
        }
    }
    
    @objc func timeHideElements () {
        if !self.channelCollection.isHidden {
            hideElements()
        }
    }
    
    @objc func hideElements() {
        self.channelCollection.isHidden = !(self.channelCollection.isHidden)
        self.navigationController?.isNavigationBarHidden = !(self.navigationController?.isNavigationBarHidden)!
        self.btnPlayPause.isHidden = !(self.btnPlayPause.isHidden)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return channels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCollectionViewCell", for: indexPath) as? ChannelCollectionViewCell {
            cell.configureCell(channel: self.channels[indexPath.row])
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.channel?.name != self.channels[indexPath.row].name {
            self.channel = self.channels[indexPath.row]
            changePlayChannel()
            if isConnected() {
                let alert = UIAlertController(title: "Transmitir", message: "", preferredStyle: UIAlertControllerStyle.alert)
                let addAction = UIAlertAction(title: "Ok", style: .default, handler: { alertAction in
                    self.sendVideo()
                })
                
                let addCancelAction = UIAlertAction(title: "\(self.kCancelTitle)", style: .cancel, handler: { alertAction in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(addAction)
                alert.addAction(addCancelAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func controllStream(_ sender: Any) {
        if self.isPlaying {
            if !isConnected() {
                pause()
            } else {
                mediaControlChannel.pause()
            }
            self.btnPlayPause.setImage(#imageLiteral(resourceName: "btn-play"), for: .normal)
            isPlayingUpdate()
        } else {
            if !isConnected() {
                play()
            } else {
                mediaControlChannel.play()
            }
            self.btnPlayPause.setImage(#imageLiteral(resourceName: "btn-pause"), for: .normal)
            isPlayingUpdate()
        }
    }
    
    func play() {
        self.playerLayer.player?.play()
    }
    
    func pause() {
        self.playerLayer.player?.pause()
    }
    
    func isPlayingUpdate() {
        self.isPlaying = !self.isPlaying
    }
    
    @IBAction func signOutPressed(_ sender: Any) {
        pause()
        
        let userDefaults = UserDefaults.standard
        userDefaults.set(false, forKey: "loginStatus")
        userDefaults.synchronize()
        
        self.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "LoginVC", sender: nil)
    }
    
    
    //*****************************************************************
    // MARK: - Side Menu
    //*****************************************************************
    
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.menuPresentMode = .menuSlideIn
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "MenuVC" {
            if let navController = segue.destination as? UISideMenuNavigationController {
                if let menuController = navController.viewControllers.first as? MenuVC {
                    if let ch = self.channel {
                        menuController.channels = [ch]
                    }
                }
            }
        }
    }
    
    @IBAction func btnMenuPressed(_ sender: Any) {
        performSegue(withIdentifier: "MenuVC", sender: self)
    }
    
    //*****************************************************************
    // MARK: - AVAudio Sesssion Interrupted
    //*****************************************************************
    
    // Example code on handling AVAudio interruptions (e.g. Phone calls)
    func sessionInterrupted(notification: NSNotification) {
        if let typeValue = notification.userInfo?[AVAudioSessionInterruptionTypeKey] as? NSNumber{
            if let type = AVAudioSessionInterruptionType(rawValue: typeValue.uintValue){
                if type == .began {
                    print("interruption: began")
                    // Add your code here
                } else{
                    print("interruption: ended")
                    // Add your code here
                }
            }
        }
    }
    
    //*****************************************************************
    // MARK: - Chromecast
    //*****************************************************************
    
    func updateButtonStates() {
        if (deviceScanner?.devices.count == 0) {
            //Hide the cast button
            self.navigationItem.rightBarButtonItems = [logout]
        } else {
            // Show the Cast button.
            self.navigationItem.rightBarButtonItems = [googleCastButton, logout]
            if isConnected() {
                //Show cast button in enabled state
                self.googleCastButton.image =  #imageLiteral(resourceName: "chrmcastConec")
//                self.googleCastButton.tintColor = UIColor.blue
            } else {
                //Show cast button in disabled state
                self.googleCastButton.image = #imageLiteral(resourceName: "chrmcastDesc")
//                self.googleCastButton.tintColor = UIColor.gray
            }
        }
    }
    
    func connectToDevice() {
        if (selectedDevice == nil) {
            return
        }
        
        let identifier = Bundle.main.bundleIdentifier
        deviceManager = GCKDeviceManager(device: selectedDevice!, clientPackageName: identifier!)
        deviceManager!.delegate = self
        deviceManager!.connect()
    }
    
    func deviceDisconnected() {
        selectedDevice = nil
        deviceManager = nil
    }
    
    func showError(error: NSError) {
        let alert = UIAlertController(title: "Erro", message: error.description,
                                      preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func chooseDevice(_ sender: Any) {
        if selectedDevice == nil {
            let alert = UIAlertController(title: "Transmitir", message: "Escolha o seu dispositivo", preferredStyle: UIAlertControllerStyle.actionSheet)
            if let deviceScanner = deviceScanner {
                deviceScanner.passiveScan = false
                for device in deviceScanner.devices  {
                    alert.addAction(UIAlertAction(title: (device as AnyObject).friendlyName, style: UIAlertActionStyle.default, handler: { alertAction in
                        self.selectedDevice = device as? GCKDevice
                        self.connectToDevice()
                    }))
                }
            }
            
            let addCancelAction = UIAlertAction(title: "\(kCancelTitle)", style: .cancel, handler: { alertAction in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(addCancelAction)
            
            self.present(alert, animated: true, completion: nil)
        } else {
            updateButtonStates()
            updateStatsFromDevice()
            
            if let friendlyName = selectedDevice?.friendlyName {
                let alert = UIAlertController(title: "Transmitindo para: ", message: "\(friendlyName)", preferredStyle: UIAlertControllerStyle.actionSheet)
                if let info = mediaInformation {
                    alert.addAction(UIAlertAction(title: info.metadata?.object(forKey: kGCKMetadataKeyTitle) as? String, style: UIAlertActionStyle.default, handler: nil))
                }
                
                let addCancelAction = UIAlertAction(title: "\(kCancelTitle)", style: .cancel, handler: { alertAction in
                    alert.dismiss(animated: true, completion: nil)
                })
                alert.addAction(addCancelAction)
                alert.addAction(UIAlertAction(title: kDisconnectTitle, style: UIAlertActionStyle.destructive, handler: { alertAction in
                    self.deviceManager!.disconnect();
                    if self.isPlaying {
                        self.playerLayer.player?.play()
                    }
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func updateStatsFromDevice() {
        if isConnected() && mediaControlChannel.mediaStatus != nil {
            mediaInformation = mediaControlChannel.mediaStatus?.mediaInformation
        }
    }
    
    func isConnected() -> Bool {
        if let manager = deviceManager {
            return manager.connectionState == GCKConnectionState.connected
        } else {
            return false
        }
    }
    
    func sendVideo() {
        // Define Media Metadata.
        let metadata = GCKMediaMetadata();
        if let channelName = self.channel?.name {
            metadata.setString("\(channelName)", forKey: kGCKMetadataKeyTitle);
        } else {
            metadata.setString("Transmitindo", forKey: kGCKMetadataKeyTitle);
        }
        metadata.setString("preparando para transmitir o canal.", forKey:kGCKMetadataKeySubtitle);
        metadata.addImage(GCKImage(url: URL(string: (self.channel?.url_logo)!)!, width: 480, height: 360))
        
        // Define Media Information.
        if let url = self.channel?.url {
            print("castURL - \(String(describing: url))")
            let mediaInformation = GCKMediaInformation(
                contentID: "\(url)",
                streamType: GCKMediaStreamType.live,
                contentType: "m3u8",
                metadata: metadata,
                streamDuration: 0,
                mediaTracks: [],
                textTrackStyle: nil,
                customData: nil
            )
            // Cast media.
            mediaControlChannel.loadMedia(mediaInformation, autoplay: true);
        }
    }
    
    // MARK: GCKDeviceScannerListener
    func deviceDidComeOnline(_ device: GCKDevice) {
        print("Dispositivo encontrado: \(String(describing: device.friendlyName))");
        updateButtonStates();
    }
    
    func deviceDidGoOffline(_ device: GCKDevice) {
        print("Dispositivo fora do alcance: \(String(describing: device.friendlyName))");
        updateButtonStates();
    }
    
    // MARK: GCKDeviceManagerDelegate
    func deviceManagerDidConnect(_ deviceManager: GCKDeviceManager) {
        print("Conectado.");
        updateButtonStates();
        deviceManager.launchApplication(kReceiverAppID);
    }

    func deviceManager(_ deviceManager: GCKDeviceManager, didConnectToCastApplication applicationMetadata: GCKApplicationMetadata, sessionID: String, launchedApplication: Bool) {
        print("O aplicativo iniciou.");
        deviceManager.add(self.mediaControlChannel)
        
        let alert = UIAlertController(title: "Começar a trasmitir?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        let addAction = UIAlertAction(title: "Ok", style: .default, handler: { alertAction in
            self.player?.pause()
            self.sendVideo()
            alert.dismiss(animated: true, completion: nil)
        })
        
        let addCancelAction = UIAlertAction(title: kCancelTitle, style: .cancel, handler: { alertAction in
            alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(addAction)
        alert.addAction(addCancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func deviceManager(_ deviceManager: GCKDeviceManager, didFailToConnectToApplicationWithError error: Error) {
        print("Notificação recebida que o dispositivo falhou ao conectar-se ao aplicativo.");
        showError(error: error as NSError);
        deviceDisconnected();
        updateButtonStates();
    }
    
    func deviceManager(_ deviceManager: GCKDeviceManager, didFailToConnectWithError error: Error) {
        print("Notificação recebida que o dispositivo falhou ao conectar.");
        showError(error: error as NSError);
        deviceDisconnected();
        updateButtonStates();
    }
    
    func deviceManager(_ deviceManager: GCKDeviceManager, didDisconnectWithError error: Error?) {
        print("Notificação recebida de que o dispositivo foi desconectado.");
        if (error != nil) {
            showError(error: error! as NSError)
        }
        deviceDisconnected();
        updateButtonStates();
    }
    
    //*****************************************************************
    // MARK: - Network Connection
    //*****************************************************************
    
    func connectionObserver() {
        // Connection
        if let reachability = AppDelegate.sharedAppDelegate()?.reachability {
            NotificationCenter.default.addObserver( self, selector: #selector( self.reachabilityChanged ),name: ReachabilityChangedNotification, object: reachability )
        }
    }

    
    @objc private func reachabilityChanged( notification: NSNotification ) {
        guard let reachability = notification.object as? Reachability else {
            return
        }
        
        connectionObserver()
    
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Conectado via WiFi")
                if isPlaying {
                    changePlayChannel()
                }
            } else {
                print("Conectado via Dados móveis")
            }
        } else {
            print("Rede não pode se conectar")
            isPlayingUpdate()
        }
    }

    //*****************************************************************
    // MARK: - Verificar depois
    //*****************************************************************

    
//    func playerDidReachEnd(notification: NSNotification) {
//        self.player?.seek(to: kCMTimeZero)
//        self.player?.play()
//    }
//
    func applicationDidEnterBackground(application: UIApplication) {
        player?.pause()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        player?.play()
    }

    func applicationWillResignActive(application: UIApplication) {
        player?.pause()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        player?.play()
    }
}
