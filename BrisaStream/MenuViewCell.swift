//
//  MenuViewCell.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 06/09/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var logo_img: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    let channel_activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    func configureCell(channel: Channel) {
        self.titleLabel.text = channel.name
        self.descriptionLabel.text = channel.description

        let url = URL(string: channel.url_logo)!
        if(channel.flag) {
            channel_activityIndicator.center = CGPoint(x: self.logo_img.bounds.size.width/2, y: self.logo_img.bounds.size.height/2)
            
            channel_activityIndicator.hidesWhenStopped = true
            channel_activityIndicator.startAnimating()
            logo_img.addSubview(channel_activityIndicator)
            DispatchQueue.global().async {
                do {
                    let data = try Data(contentsOf: url)
                    DispatchQueue.global().sync {
                        self.logo_img.image = UIImage(data: data)
                        self.channel_activityIndicator.stopAnimating()
                        channel.flag = false
                    }
                } catch let err as NSError {
                    print(err.debugDescription)
                }
            }
        } else {
            self.logo_img.image = channel.logo.image
        }
        channel_activityIndicator.hidesWhenStopped = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
