//
//  Channel.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 21/08/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Channel {
    
//    func encode(with aCoder: NSCoder) {
//        aCoder.encode(self.name, forKey: "name")
//        aCoder.encode(self.logo, forKey: "logo")
//    }

    
    private var _id:String!
    private var _flag:Bool!
    private var _number:String!
    private var _name:String!
    public var url:URL?
    public var url_dash:URL?
    private var _url_logo:String!
    private var _logo:UIImageView!
    private var _description:String!
    
    var id: String {
        if _id == nil { _id = "" }
        return _id
    }
    
    var flag: Bool {
        set
        {
            _flag = newValue
        }
        get {
            if _flag == nil { _flag = true }
            return _flag
        }
    }

    var number: String {
        if _number == nil { _number = "" }
        return _number
    }
    
    var name: String {
        if _name == nil { _name = "" }
        return _name
    }
    
    var description: String {
        if _description == nil { _description = "" }
        return _description
    }
    
    var url_logo: String {
        if _url_logo == nil { _url_logo = "" }
        return _url_logo
    }
    
    var logo: UIImageView {
        if _logo == nil { _logo = UIImageView() }
        return _logo
    }
    
    init(channelDict: Dictionary<String, AnyObject>) {
        if let name = channelDict["name"] as? String {
            self._name = name
        }
                
        if let desc = channelDict["desc"] as? String {
            self._description = desc
        }
        
        if let url_dash = channelDict["url_dash"] as? String {
            self.url_dash = URL(string: url_dash)
        }
        
        if let url_hls = channelDict["url_hls"] as? String {
            self.url = URL(string: url_hls)
        }
        
        if let img = channelDict["img"] as? String {
            self._url_logo = ("\(BASE_URL2)\(img)")
            if let url = URL(string: self.url_logo) {
                
                let user = "brisanet"
                let password = "brisanet"
                let credentialData = "\(user):\(password)".data(using: String.Encoding.utf8)!
                let base64Credentials = credentialData.base64EncodedString(options: [])
                let headers = ["authorization": "Basic \(base64Credentials)"]
                let utilityQueue = DispatchQueue.global(qos: .utility)
                                
                Alamofire.request("\(url)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).validate().downloadProgress(queue: utilityQueue) { progress in
                        print("Download Progress: \(progress.fractionCompleted)")
                    }
                    .responseData { response in
                        print("Response is \(response)")
                        if let data = response.result.value {
                            self.logo.image = UIImage(data: data)
                            self._flag = false
                    }
                }
            }
        }
    }
}
