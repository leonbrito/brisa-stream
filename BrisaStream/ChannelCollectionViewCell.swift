//
//  ChannelCollectionViewCell.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 28/08/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit
import Alamofire

class ChannelCollectionViewCell: UICollectionViewCell {
    
//    let channel_activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)

    @IBOutlet weak var channelLogo: UIImageView!
    
    func configureCell(channel: Channel) {
        let url = URL(string: channel.url_logo)!
        if(channel.flag) {
//            channel_activityIndicator.center = CGPoint(x: self.channelLogo.bounds.size.width/2, y: self.channelLogo.bounds.size.height/2)
//            
//            channel_activityIndicator.hidesWhenStopped = true
//            channel_activityIndicator.startAnimating()
//            channelLogo.addSubview(channel_activityIndicator)
            
            let utilityQueue = DispatchQueue.global(qos: .utility)
            
            Alamofire.request("\(url)", method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).downloadProgress(queue: utilityQueue) { progress in
                print("Download Progress image: \(progress.fractionCompleted)")
                }.responseData { response in
                    print("Response is \(response)")
                    if let data = response.result.value {
                        self.channelLogo.image = UIImage(data: data)
                        channel.flag = false
                    }
            }
        } else {
            channelLogo.image = channel.logo.image
        }
//        channel_activityIndicator.hidesWhenStopped = true
    }
    
}
