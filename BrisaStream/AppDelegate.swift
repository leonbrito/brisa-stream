//
//  AppDelegate.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 18/08/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit
import ReachabilitySwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GCKLoggerDelegate {

    var window: UIWindow?
    var reachability: Reachability?
    
    override init(){
        super.init()
        GCKLogger.sharedInstance().delegate = self
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        var initialViewController = sb.instantiateViewController(withIdentifier: "Login")
        
        let userDefaults = UserDefaults.standard
        
        if userDefaults.bool(forKey: "loginStatus") {
            initialViewController = sb.instantiateViewController(withIdentifier: "PlayerVC")
        }
        
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        
        self.reachability = Reachability()
        connectionObserver()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applicationWillResignActive"), object: nil)
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applicationDidEnterBackground"), object: nil)

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "applicationDidBecomeActive"), object: nil)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // Chromecast
    func logFromFunction(function: UnsafePointer<Int8>, message: String!) {
        let functionName = String(cString:function)
        print(functionName + " - " + message);
    }
    
    // Connection
    class func sharedAppDelegate() -> AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
    
    func connectionObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: ReachabilityChangedNotification, object: nil)
        do {
            try reachability?.startNotifier()
        } catch {
            print( "ERROR: Could not start reachability notifier." )
        }
    }
    
    @objc func reachabilityChanged(notification: Notification) {
        
        if let reachability = notification.object as? Reachability {
            if reachability.isReachable {
                if reachability.isReachableViaWiFi {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Celular")
                }
            } else {
                print("Network not reachable")
                connectionObserver()
                let controller = UIAlertController(title: "Não foi detectado Internet", message: "Este aplicativo requer uma conexão com a Internet", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                
                controller.addAction(ok)
                controller.addAction(cancel)
                self.window?.rootViewController?.present(controller, animated: true, completion: nil)
            }

        }
    }
}

