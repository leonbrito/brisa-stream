//
//  ScrollVC.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 24/08/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit

class ScrollVC: UIScrollView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    */
    override func draw(_ rect: CGRect) {
        var contentWidth: CGFloat = 0.0
        let scrollWidth = self.channelScroll.frame.size.width
        for x in 0...(self.channels.count - 1) {
            var newX: CGFloat = 0.0
            newX = scrollWidth / 2 + scrollWidth * CGFloat(x)
            
            contentWidth += newX
            
            self.channelScrow.addSubview(self.channels[x].logo)
            self.channels[x].logo.frame = CGRect(x: newX, y: (self.channelScrow.frame.size.height / 2), width: 100, height: 100)
        }
        
        self.channelScroll.clipsToBounds = false
        self.channelScroll.contentSize = CGSize(width: contentWidth, height: self.view.frame.size.height)
        self.channelScroll.tag = 100
    }
 

}
