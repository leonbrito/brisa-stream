//
//  MenuVC.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 06/09/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class MenuVC: UITableViewController {

    var channels = [Channel]()
    
    override func viewDidLoad() {
        
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChannelsCell", for: indexPath) as! MenuViewCell
        cell.configureCell(channel: channels[indexPath.row])
        return cell
    }
    
}
