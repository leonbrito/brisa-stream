//
//  Constants.swift
//  BrisaStream
//
//  Created by Leonardo Brito on 21/08/17.
//  Copyright © 2017 Leonardo Brito. All rights reserved.
//

import Foundation

//let BASE_URL = "https://brisastream.herokuapp.com/"
//let URL_CHANNELS = "channels.json"
let LOGIN_URL = "https://login.brisanet.com.br/api/autenticacao/login"
let BASE_URL2 = "http://livetv.brisanet.com.br/"
let URL_CHANNELS2 = "assets/channels.json"

typealias DownloadComplete = () -> ()
